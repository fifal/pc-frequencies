#ifndef STACK_H
#define STACK_H
#define STACK_SIZE 1000
struct Stack {
    int top;
    int items[STACK_SIZE];
};
int push(int id, struct Stack *ps);
int pop(struct Stack *ps);
struct Stack *create_stack();
int free_stack(struct Stack *ps);
int is_not_in_stack(int id, struct Stack *s);
#endif
