#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "graph.h"

/** Deklerace metod **/
int parse_file(char *filename);

int init();

int is_too_close(double x1, double y1, double x2, double y2, int radius);

/** Deklerace promenych */
char **frequencies;
/* pole s moznymi frekvencemi*/
char *radius;
/*radius vysilacu*/
char **transmitters;
/*pole s vysilaci*/
int FREQ_LENGTH = 0;
/*velikost pole s frekvencemi*/
int TRAN_LENGTH = 0;
/* velikost pole s vysilaci*/
static int MAX_FREQ = 1000;
/*maximalni velikost pole s frekvencemi*/
static int MAX_TRANS = 2000;
/*maximalni velikost pole s vysilaci*/
int *adjmat;
/*matice hran*/
struct Graph g; /*graf*/

/** Struktura na uchovani pozice vysilace*/
struct Transmitter {
    double x;
    double y;
};

/** Deklerace metod */
int parse_values(struct Transmitter trans[]);

int get_adjency_matrix(int trans_length, int *adjacency_matrix, struct Transmitter trans[]);

int fill_graph(int *adjacency_matrix);

int free_all();

/**
 * Spousteci metoda main
 */
int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("ERR#1: Missing argument!\n");
        printf("USAGE\n   freq.exe [path to file with transmitters]\n\n");
        return 1;
    }
    else {
        /* Pokud nebyl dostatek pameti pro inicializaci promennych*/
        if (init() == 2) {
            printf("ERR#2: Out of memory!");
            return 2;
        }

        /* Pokud soubor neexistuje. */
        if (parse_file(argv[1]) == 4) {
            printf("ERR#4: File not found!");
            return 4;
        }
        else {
            /** Deklerace a naplneni struktury vysilace */
            struct Transmitter trans[2000];

            /* Pri nedostatku pameti*/
            if(parse_values(trans)==2){
                printf("ERR#2: Out of memory!");
                return 2;
            }

            /** Alokovani pameti pro matici hran a jeji naplneni */
            adjmat = malloc(sizeof(int) * TRAN_LENGTH * TRAN_LENGTH);
            if (adjmat == NULL) {
                printf("ERR#2: Out of memory!");
                return 2;
            }
            get_adjency_matrix(TRAN_LENGTH, adjmat, trans);

            /** Naplneni grafu podle matice hran */
            if(fill_graph(adjmat)==2){
                printf("ERR#2: Out of memory!");
                return 2;
            };

            /** Pokud pri vytvareni grafu a prirazovani neexistuje reseni */
            if (color_graph(g, FREQ_LENGTH) == 3 || print_graph(g, frequencies) == 3) {
                printf("ERR#3: Non-existing solution!");
                free_all();
                return 3;
            }
            else if (color_graph(g, FREQ_LENGTH) == 5) {
                printf("#ERR#5: Stack overflow!");
                return 5;
            }
            else {
                free_all();
                return 0;
            }
        }
    }
}

/**
 * Funkce inicializujici promenne
 */
int init() {
    int i, j;
    frequencies = malloc(sizeof(char *) * (1 + MAX_FREQ));
    transmitters = malloc(sizeof(char *) * (1 + MAX_TRANS));
    radius = malloc(sizeof(char) * 10);
    if (frequencies == NULL || transmitters == NULL || radius == NULL) {
        return 2;
    }

    for (i = 0; i < MAX_FREQ; i++) {
        frequencies[i] = malloc(sizeof(char) * 128);
        memset(frequencies[i], ' ', 1);
    }
    for (j = 0; j < MAX_TRANS; j++) {
        transmitters[j] = malloc(sizeof(char) * 128);
        memset(transmitters[j], ' ', 1);
    }
    return 0;
}

/**
 * Vyparsuje hodnoty ze souboru a ulozi je do pole
 *
 * @param filename nazev souboru
 * @return 4 pokud soubor neexistuje
 */
int parse_file(char *filename) {
    char buf[256];
    int action = 0;
    int i = 0;
    int j = 0;
    int k = 0;
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        return 4;
    }

    while (fgets(buf, sizeof(buf), file)) {
#ifdef _WIN32
        if (!strcmp(buf, "Available frequencies:\n")) {
            action = 0;
        }
        else if (!strcmp(buf, "Transmission radius:\n")) {
            action = 1;
        }
        else if (!strcmp(buf, "Transmitters:\n")) {
            action = 2;
        }
#else
        if (!strcmp(buf, "Available frequencies:\r\n")) {
            action = 0;
        }
        else if (!strcmp(buf, "Transmission radius:\r\n")) {
            action = 1;
        }
        else if (!strcmp(buf, "Transmitters:\r\n")) {
            action = 2;
        }
#endif
        switch (action) {
            case 0:
                if (i == 0) {
                    i++;
                }
                else {
                    strcpy(frequencies[i - 1], buf);
                    FREQ_LENGTH++;
                    i++;
                }
                break;
            case 1:
                if (j == 0) {
                    j++;
                }
                else {
                    strcpy(radius, buf);
                    j++;
                }
                break;
            case 2:
                if (k == 0) {
                    k++;
                }
                else {
                    strcpy(transmitters[k - 1], buf);
                    TRAN_LENGTH++;
                    k++;
                }
                break;
        }
    }
    fclose(file);
    return 0;
}

/**
 * Vyparsuje hodnoty co jsou v poli. Napriklad u frekvence misto stringu "ID cislo_frekvence" zustane pouze
 * "cislo_frekvence". U pozice vysilacu misto "ID xova_souradnice yova_souradnice" zustane pouze
 * "xova_souradnice yova_souradnice".
 *
 * @param trans[] pole struktur vysilace
 */
int parse_values(struct Transmitter trans[]) {
    int i, j;
    char *frequency = malloc(sizeof(char) * 256);
    char *freq_start = frequency;

    char *x = malloc(sizeof(char) * 256); /* Alokace paměti */
    char *y = malloc(sizeof(char) * 256);
    char *x_start = x; /* Uložení ukazatale na počátek, funkce strtok ukazatel upravuje, pro následné uvolnění paměti */
    char *y_start = y;

    if(frequency == NULL || x == NULL || y == NULL){
        return 2;
    }
    /* Vraci do pole hodnoty frekvenci bez ID frekvence*/
    for (i = 0; i < FREQ_LENGTH; i++) {
        strcpy(frequency, frequencies[i]);

        /*ziskani pouze frekvence*/
        frequency = strtok(frequency, " ");
        frequency = strtok(NULL, " ");

        /*nakopirovani do pole*/
        strcpy(frequencies[i], frequency);
        frequency = freq_start;
    }

    /* Vraci do struktury souradnice vysilace*/
    for (j = 0; j < TRAN_LENGTH; j++) {
        strcpy(x, transmitters[j]);
        strcpy(y, transmitters[j]);

        /*xova souradnice vysilace plus pridani mezery na konec stringu*/
        x = strtok(x, " ");
        x = strtok(NULL, " ");
        /*strcat(x, " ");*/

        /*yova souradnice vysilace*/
        y = strtok(y, " ");
        y = strtok(NULL, " ");
        y = strtok(NULL, " ");

        /*nakopirovani yove souradnice za xovou a vlozeni do pole a nastaveni pocatecni frekvence (neobarveny)*/
        trans[j].x = strtod(x, NULL);
        trans[j].y = strtod(y, NULL);
        x = x_start;
        y = y_start;

    }
    free(x_start);
    free(y_start);
    free(freq_start);
    return 0;
}

/**
 * Vytvori matici hran, vlozi 1 pokud vysilace koliduji, jinak vlozi 0
 *
 * @param trans_length delka pole s vysilaci
 * @param adjacency_matrix matice sousedu
 * @param trans[] struktura s vysilaci
 */
int get_adjency_matrix(int trans_length, int *adjacency_matrix, struct Transmitter trans[]) {
    int i, j;
    for (i = 0; i < trans_length; i++) {
        for (j = 0; j < trans_length; j++) {
            if (i != j &&
                is_too_close(trans[i].x, trans[i].y, trans[j].x, trans[j].y, (int) strtol(radius, NULL, 0)) ==
                1) { /* Pokud spolu vysilace koliduji */
                *(adjacency_matrix + i * trans_length + j) = 1;
            }
            else {
                *(adjacency_matrix + i * trans_length + j) = 0;
            }
        }
    }
    return 0;
}

/**
 * Funkce pro zjisteni jestli vysilace koliduji
 *
 * @param x1 xova souradnice prvniho vysilace
 * @param y1 yova souradnice prvniho vysilace
 * @param x2 xova souradnice druheho vysilace
 * @param y2 yova souradnice druheho vysilace
 * @param radius radius vysilace
 *
 * @return 0 pokud vysilace nekoliduji, 1 pokud koliduji
 */
int is_too_close(double x1, double y1, double x2, double y2, int radius) {
    double first = pow(x2 - x1, 2);
    double second = pow(y2 - y1, 2);
    double distance = sqrt(first + second);
    if (distance > (radius * 2)) {
        return 0;
    }
    else {
        return 1;
    }
}

/**
 * Vytvori graf, prida vrcholy, a kazdemu vrcholu priradi sousedy podle matice sousednosti
 *
 * @param adjacency_matrix matice sousedu
 */
int fill_graph(int *adjacency_matrix) {
    int i, j, k;
    g = create_graph(TRAN_LENGTH);
    for (i = 0; i < TRAN_LENGTH; i++) {
        add_vertex(i, g);
    }
    for (j = 0; j < TRAN_LENGTH; j++) {
        for (k = 0; k < TRAN_LENGTH; k++) {
            if (*(adjacency_matrix + j * TRAN_LENGTH + k) == 1) {
                if(add_vertex_neighbor(j, k, g)==2){
                    return 2;
                }
            }
        }
    }
    return 0;
}

/**
 * Funkce pro uvolneni pameti
 */
int free_all() {
    int i, j;
    for (i = 0; i < MAX_FREQ; i++) {
        free(frequencies[i]);
    }
    for (j = 0; j < MAX_TRANS; j++) {
        free(transmitters[j]);
    }
    free(frequencies);
    free(transmitters);
    free(radius);
    free(adjmat);
    free_graph(g);
    return 0;
}