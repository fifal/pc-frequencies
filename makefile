CC = gcc
CFLAGS = -Wall -pedantic -ansi -lm
BIN = freq.exe
OBJ = main.o graph.o stack.o

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

$(BIN): $(OBJ)
	$(CC) $^ -lm -o $@

