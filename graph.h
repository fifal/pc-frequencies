#ifndef GRAPH_H
#define GRAPH_H
struct Graph create_graph(int n_vertexes);
struct Graph {
    int n; /*pocet vrcholu*/
    struct Vertex *vertexes; /*list vrcholu*/
};

int add_vertex(int id, struct Graph g);
struct Vertex {
    int id; /*id vrcholu*/
    int color; /*barva vrcholu*/
    int n; /*pocet sousedu*/
    struct Vertex *neighbors; /*sousede*/
};
int add_vertex_neighbor(int id, int id_neighbor, struct Graph g);
int print_graph(struct Graph g, char **frequencies);
int color_graph(struct Graph g, int available_freq);
int free_graph(struct Graph g);
int check_graph(struct Graph g);
int get_lowest_freq(struct Graph g, int id, int available_freq);
#endif
